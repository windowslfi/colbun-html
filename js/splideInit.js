document.addEventListener("DOMContentLoaded", function () {
  generateCarousel(".sv-splide-news", {
    type   : 'loop',
    perPage: 3,
    pagination: true,
    gap: 49,
    breakpoints: {
      540: {
        perPage: 1,
      },
      800:{
        gap: 20,
      }
    }
  });
  generateCarousel(".sv-splide-tutorial", {
    type   : 'loop',
	  perPage: 1,
    pagination: false,
    gap: 20,
    breakpoints: {
      540: {
        perPage: 1,
      },
      800:{
        gap: 20,
      }
    }
  });
});

function generateCarousel(className, options) {
  let splideClassName = document.querySelector(className)
  if(splideClassName){
    new Splide(className, options).mount();
  }
}
